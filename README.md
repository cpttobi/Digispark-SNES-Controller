# Digispark-SNES-Controller

Use a [Digispark][1] (ATTiny85 dev board with [micronucleus][2] bootloader) to connect a Super Nintendo (SNES) controller to a computer via USB.

[1]: http://digistump.com/products/1
[2]: https://github.com/micronucleus/micronucleus

## Wiring

The SNES controller has a 7 pin connector, but only 5 of those are connected. This diagram should help you with wiring it up to your Digispark:

```
 _________________
| o o o o | o o o )
 ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
  1 2 3 4   5 6 7

Pin | Description  | Short | Wire color (usually)
 1    5 Volt supply  5V      White
 2    Data Clock     CLK     Yellow
 3    Data Latch     PS      Orange
 4    Serial Data    DAT     Red
 5    Not Connected  -       -
 6    Not Connected  -       -
 7    Ground         GND     Brown
```

Connect +5V and GND to the corresponding pins on your Digispark and the other 3 pins to pins 0, 1, and 2 (you can also use pin 5, but not 3 and 4, they are used for USB).  
Then configure the pin numbers (DAT, PS, and CLK) in the Arduino source code to match how you connected them.

## License
This program is licensed under the GPL v3.

See LICENSE.txt for details.

## Credits

Original code by waveOutReset.
Forked and modified by Tobi.

