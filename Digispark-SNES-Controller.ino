#include "DigiJoystick.h"

// Configure pins
#define DAT 2
#define PS 1
#define CLK 0

#define PULSE_TIME 3

// Pulse function for SNES controller
void pulseClock(byte pin) {
  digitalWrite(pin, HIGH);
  delayMicroseconds(PULSE_TIME);
  digitalWrite(pin, LOW);
  delayMicroseconds(PULSE_TIME);
}

// Read the buttons and axes
char* readInput(char buf[8]) {
  pulseClock(CLK);
  
  // Pulse the Latch line to "collect" the input on the SNES controller
  pulseClock(PS);

  // Retrieve the input from the SNES controller
  // B Button
  if (!digitalRead(DAT)) buf[6] |= 0x04;
  pulseClock(CLK);
  // Y Button
  if (!digitalRead(DAT)) buf[6] |= 0x08;
  pulseClock(CLK);
  // Select Button
  if (!digitalRead(DAT)) buf[6] |= 0x80;
  pulseClock(CLK);
  // Start Button
  if (!digitalRead(DAT)) buf[6] |= 0x40;
  pulseClock(CLK);
  // Up
  if (!digitalRead(DAT)) buf[1] -= 128;
  pulseClock(CLK);
  // Down
  if (!digitalRead(DAT)) buf[1] += 127;
  pulseClock(CLK);
  // Left
  if (!digitalRead(DAT)) buf[0] -= 128;
  pulseClock(CLK);
  // Right
  if (!digitalRead(DAT)) buf[0] += 127;
  pulseClock(CLK);
  // A Button
  if (!digitalRead(DAT)) buf[6] |= 0x02;
  pulseClock(CLK);
  // X Button
  if (!digitalRead(DAT)) buf[6] |= 0x01;
  pulseClock(CLK);
  //  L Button
  if (!digitalRead(DAT)) buf[6] |= 0x10;
  pulseClock(CLK);
  // R Button
  if (!digitalRead(DAT)) buf[6] |= 0x20;

  return buf;
}

void setup() {
  // Setup the SNES controller
  pinMode(DAT, INPUT);
  pinMode(PS, OUTPUT);
  pinMode(CLK, OUTPUT);
  digitalWrite(PS, LOW);
  digitalWrite(CLK, LOW);
 
  // Reset the axes as workaround for weird bug somewhere in the USB stuff
  char buf[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  DigiJoystick.delay(1000);
  DigiJoystick.setValues(buf);
  DigiJoystick.delay(100);
}

void loop() {
  // Data being sent to the USB library
  char buf[8] = {
    // 6 axes (one byte each)
    0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    // 16 buttons (1 bit each)
    0x00, 0x00
  };

  // Read input and send it to the USB library
  DigiJoystick.setValues(readInput(buf));
  DigiJoystick.delay(1);
}
